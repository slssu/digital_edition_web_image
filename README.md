## Table of Contents

- [Table of Contents](#table-of-contents)
- [About](#about)
- [Dependencies](#dependencies)
- [Get Started](#get-started)
- [Change Template (landing page)](#change-template-landing-page)
- [Add Images](#add-images)
- [Useful Docker Commands](#useful-docker-commands)
- [MySQL Workbench](#mysql-workbench)

## About
This repository is used for deploying all three services that are required for project *Topelius* using Docker.
All services can be found in docker-compose.yml.
Each service has its own dependencies and files in a docker-* folder.

Required-files folder contains md, xml, xslt, images, cache and facsimiles.

## Dependencies 
* [Docker](https://www.docker.com/get-docker)
* [Docker Compose](https://docs.docker.com/compose/install/)

## Get Started
`git clone -b db2018 git@bitbucket.org:slssu/topelius_files.git`

`cd topelius_files`

`docker-compose up`

## Change Template (landing page)
The service *docker-web* comes with a default landing page, but it is possible to change it. Templates can be found at `/docker-web/templates`, there is even an example template called *template_example.html* .
To change template all that you need to is to go to `/docker-web/Dockerfile` and uncomment this line `COPY templates/template_example.html src/pages/home/home.html` and make sure that the name of the template file is correct.

## Add Images
Make sure that the image you want to add exists in `/docker-web/assets/images`, if it doesn't please put it there.

Now if we want to add an image to a markdown file it can either be done using markdown:
```
![My image](assets/images/my_image.jpg "This is will displayed when hovering")
```
Or it can be done with simple html (it is possible to add html to markdown):
```
<img style="" src="assets/images/my_image.jpg">
```

## Useful Docker Commands
* Build, (re)create, start and attach containers for service(s) `docker-compose up`
    * Detached mode `docker-compose up -d`
* List images `docker images`
* List containers `docker ps`
    * Only running containers `docker ps -a`
* Remove one or more containers `docker rm <container>`
* Remove one or more images `docker rmi <docker-image>`
* Create bash session in container `docker exec -it <container> bash`
* [All Docker commands](https://docs.docker.com/engine/reference/commandline/docker/)
* [All Docker Compose commands](https://docs.docker.com/compose/reference/)

## MySQL Workbench
If you want to use MySQL Workbench to connect to our MySQL database running in a docker container it can be done.

* Create a new connection
* Hostname = 127.0.0.1
* Port = 3306
* Username = root
* Password = somepassword